CAP5400 (Fall 2012) Assignment 3 - Circle Detection Using Hough Transform

Author: Carlos Ezequiel

Directory structure
-------------------

This software is architectured as follows

/iptools -This folder hosts the files that are compiled into a static library. 
	image - This folder hosts the files that define an image.
	utility - this folder hosts the files that students store their implemented algorithms.
	video - This folder hosts the files that define an video.
	
/lib- This folder hosts the static libraries associated with this software.

/project - This folder hosts the files that will be compiled into executables.
	/bin - This folder hosts the binary executables created in the project directory.
    /tests - This folder contains test files
        /input - Source image and video files
        /output - Processed image and video files are dumped here by the test
                  script
        /conf - Contains test parameter files
        edge_detect_test.py - Auto-generates test scripts and parameter files

/doc - Contains documentation about this project
	

Compiling the program 
----------------------

To compile this software enter the /project directory and run `make`.


Running the program 
----------------------

The program may be run from /project/bin directory.
Program syntax:

$ ./iptool [-s -g] SRC TGT PARAM

where:
    SRC - source image/video file
    TGT - output image/video file
    PARAM - Parameter file

Options:
    -g      Force greyscale frame rendering (for video only)
    -s      Use sequence-of-interest (SOI) processing. This is mainly used
            for obtaining the histogram using all the frames within the SOI
            of the video

The following image file/s are supported: .ppm, .pgm
The following video file/s are supported: .m1v, .mpeg

If SRC is a video file, TGT is an image file, and -s is given, the program
will generate a histogram image for each ROI across each corresponding
SOI of the video, as given in the parameter file. The image will be taken
from the first frame of the first SOI specified in the parameter file.

Running the program without any arguments prints the syntax for the program

The parameter file format specification is located in /doc.


Running the tests
-----------------

To run the tests, compile the program first.
Then, generate the tests. Go to /projects/tests and execute

$ make

Or, generate the tests using the Python script directly:

$ ./edge_detect_test.py

This should result in two test scripts:

run_img.sh      Runs image tests

Also, the parameter file for each test will be generated in tests/conf
directory.

Run the tests in a bash shell:

$ ./run_img.sh

The scripts will print out each test being performed, and after
completion, the time it took to process each test.

The output image files are stored in /tests/output. They are organized as follows:

/center.d       Contains binary images showing possible circle centers
/detected.d     Contains images that have been overlayed with detected circles
/dir.d          Contains gradient direction images
                gradient (refer to report in /doc for more information)
/edge.d         Contains binary edge images (gradient images after thresholding was applied)
/grad.d         Contains gradient magnitude images
/hough.d        Contains Hough space images

Report
------
## Introduction ##
The goal of this assignment is to investigate detection of circles in images using Hough transform method. Hough transform is a feature extraction technique that is used to find rough instances of objects having a particular shape. The objects are identified using a voting procedure carried out in parameter space. Object candidates are derived from local maxima (i.e. maximum number of votes in a region) in the accumulator space. The accumulator is a matrix representation of parameter space, that is constructed by the Hough transform algorithm.
An edge detection method is used to extract a binary edge image needed by the Hough Transform. For this assignment, the Sobel operator is used for edge detection.

## Conclusion ##
This assignment demonstrated the basic concepts of circle detection using Hough transform. The test results were able to show the process of detecting circles on greyscale and color images. Thresholding was used in order to get better edge images for the Hough algorithm, and for filtering local maxima.
The selection of parameters for both edge detection and Hough transform proved to be crucial. Parameter selection was done empirically through multiple test runs, such that only one parameter was changed at a time. A few common parameter values appeared to be generally satisfactory across different images. Some of the common parameter traits include: a relatively high value for edge threshold T, TD was disabled (i.e. TD = −1), TR set to a value above 80, Na and Nb set to image width and image height (in pixels) respectively, Nr ≥ 10, Rmin and Rmax bound the smallest and largest radius of the circular features respectively.
An improvement to the Hough algorithm used in the assign- ment would be to expand the search range to [±θ − ε, ±θ + ε] in order to consider both possible locations of the center of an edge pixel. Another improvement would be to use a different thresholding scheme (such as weighted variable threshold) for refinement of both edge images and the Hough space images.