#! /usr/bin/python

from iptest import *

# Input files
SRC_IMG_GREY = "input/twocoins.pgm"
SRC_IMG_GREY2 = "input/coins.pgm"
SRC_IMG_GREY3 = "input/corridor.pgm"
SRC_IMG_GREY3 = "input/steve_jobs.pgm"
SRC_IMG_COLOR = "input/billiard_balls.ppm"
SRC_IMG_COLOR2 = "input/pokeballs.ppm"
SRC_IMG_COLOR3 = "input/google_office.ppm"
SRC_IMG_COLOR4 = "input/afghan_girl.ppm"
SRC_IMG_COLOR5 = "input/coins.ppm"

def greyTests(tr):

    if 1:
        tgt = "grey_two_coins_delta_edge"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [i * 25, -1, 84, 201, 179, 10, 65, 55]), 
                                   ROI(5,5,201,179))]), ""))

    if 1:
        tgt = "grey_two_coins_delta_edge_dir"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, i * 36, 84, 201, 179, 10, 65, 55]), 
                                   ROI(5,5,201,179))]), ""))

    if 1:
        tgt = "grey_two_coins_delta_hough_thresh"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 60 + i * 4, 201, 179, 10, 65, 55]), 
                                   ROI(5,5,201,179))]), ""))

    if 1:
        tgt = "grey_two_coins_delta_hough_bin"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 84, 201/(i+1), 179/(i+1), 1, 60, 60]), 
                                   ROI(5,5,201,179))]), ""))
    if 1:
        tgt = "grey_two_coins_delta_hough_radius"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 84, 201, 179, 1, (i+1) * 10, (i+1) * 10]), 
                                   ROI(5,5,201,179))]), ""))

    if 1:
        tgt = "grey_two_coins_ideal"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 84, 201, 179, 10, 65, 55]), 
                                   ROI(5,5,201,179))]), ""))

    if 1:
        tgt = "grey_coins"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_GREY2, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 90, 240, 206, 1,(i + 1)* 10, (i + 1)* 10]), 
                                   ROI(5,5,240,206))]), ""))

def colorTests(tr):

    if 1:
        tgt = "color_billiard_balls_ideal_half"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [225, -1, 96 , 120, 150, 20, 100, 40]), 
                                   ROI(130,10,120,150))]), ""))

    if 1:
        tgt = "color_billiard_balls_ideal_half_delta_hough_radius"
        for i in range(10):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [225, -1, 96 , 120, 150, 1, (i+1)*10, (i+1)*10]), 
                                   ROI(130,10,120,150))]), ""))

    if 1:
        tgt = "color_billiard_balls_ideal"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [225, -1, 96 , 240, 155, 20, 100, 40]), 
                                   ROI(5,5,240,155))]), ""))

    if 1:
        tgt = "color_room_ideal"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            #TH = 88
            tr.add(IPTest(SRC_IMG_COLOR3, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [75, 120, 88, 240/10, 132/10, 10, 30, 20]), 
                                   ROI(5,5,240,132))]), ""))

    if 1:
        tgt = "color_afghan_girl"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR4, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 80, 240/9, 225/9, 10, 20, 5]), 
                                   ROI(5,5,240,225))]), ""))

    if 1:
        tgt = "color_pokeballs"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR2, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 75, 240/5, 128/5, 30, 50, 7]), 
                                   ROI(5,5,240,128))]), ""))

    if 1:
        tgt = "color_coins_ideal"
        for i in range(1):
            tgt_i = tgt + ("%03d" % i)
            tr.add(IPTest(SRC_IMG_COLOR5, "output/"+ tgt_i, IPParams("conf/" + tgt_i + ".p",
                          [IPParam(IPFilter("edgedetect", [250, -1, 60, 240, 174, 21, 50, 15]), 
                                   ROI(5,5,240,174))]), ""))


if __name__ == '__main__':

    # Preprocess
    fp = open("run_img.sh", "w")
    fp.write("cd output && ./clean.sh && cd ..\n")
    fp.close()

    tr = IPTestRunner()

# Parameters
# 0 - edge threshold
# 1 - edge direction in degrees
# 2 - tolerance for hough local maxima (threshold = tolerance * global maxima
# 3 - number of 'a' bins
# 4 - number of 'b' bins
# 5 - number of 'r' bins
# 6 - maximum radius r

    greyTests(tr)
    colorTests(tr)

    tr.save("run_img.sh")

    # Postprocess
    fp = open("run_img.sh", "a")
    fp.write("cd output && ./sort.sh && cd ..\n")
    fp.close()


