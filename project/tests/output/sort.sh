#! /bin/sh

# Organize the output files
mkdir -p edge.d && mv *_edge.* edge.d
mkdir -p hough.d && mv *_hough.* hough.d
mkdir -p grad.d && mv *_grad.* grad.d
mkdir -p dir.d && mv *_dir.* dir.d
mkdir -p detected.d && mv *_detected.* detected.d 
mkdir -p center.d && mv *_center.* center.d 
