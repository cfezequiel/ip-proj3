// ipparams.h: Head file for IPParams class
#ifndef IPPARAMS_H
#define IPPARAMS_H

#include <string>
#include <vector>

class ROI
{
public:
    int x;
    int y;
    int sx;
    int sy;
};

class SOI
{
public:
    int beg;
    int end;
};
        
class IPParam
{
public:
    ROI roi;
    SOI soi;
    std::string filter;
    std::vector<int> filter_params;
};

std::vector<IPParam> loadIPParams(char *filename);

#endif // IPPARAMS_H
