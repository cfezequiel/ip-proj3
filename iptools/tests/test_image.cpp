#include <stdlib.h>
#include "image.h"

int main(int argc, char **argv)
{
    Image *img;
    string srcPath(argv[1]);
    COLORSPACE cs = (COLORSPACE) atoi(argv[2]);

    img = load(srcPath.c_str(), cs);
    printf("Image type: %d\n", img->getColorspace());

    return 0;
}
