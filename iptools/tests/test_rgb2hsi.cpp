#include <stdio.h>
#include <iostream>
#include <sstream>
#include "../image/image.h"

#define MAXLEN 256

int UT_rgb2hsi(int r, int g, int b, double exp_h, double exp_s, double exp_i)
{
    double h, s, i;
    int res = 0;
    rgb2hsi(r, g, b, &h, &s, &i);

    stringstream ss;
    if (h != exp_h)
    {
        ss << "h (" << h << ") != exp_h (" << exp_h << ") ";
        res = 1;
    }
    if (s != exp_s)
    {
        ss << "s (" << s << ") != exp_s (" << exp_s << ") ";
        res = 1;
    }
    if (i != exp_i)
    {
        ss << "i (" << i << ") != exp_i (" << exp_i << ") ";
        res = 1;
    }

    if (res)
    {
        printf("Error in UT_rgb2hsi(%d, %d, %d)\n", r, g, b);
        cout << ss.str() << endl << endl;
    }

    return res;
}

int UT_rgb2hsi2rgb(int r, int g, int b)
{
    double h, s, i;
    int out_r, out_g, out_b;
    int res = 0;

    rgb2hsi(r, g, b, &h, &s, &i);
    hsi2rgb(h, s, i, &out_r, &out_g, &out_b);

    stringstream ss;
    if (r != out_r)
    {
        ss << "r (" << r << ") != out_r (" << out_r << ") ";
        res = 1;
    }
    if (g != out_g)
    {
        ss << "g (" << g << ") != out_g (" << out_g << ") ";
        res = 1;
    }
    if (b != out_b)
    {
        ss << "b (" << b << ") != out_b (" << out_b << ") ";
        res = 1;
    }

    if (res)
    {
        printf("Error in UT_rgb2hsi2rgb(%d, %d, %d)\n", r, g, b);
        cout << ss.str() << endl << endl;
    }

    return res;
}

int UT_hsi2rgb(double in_h, double in_s, double in_i, int exp_r, int exp_g, int exp_b)
{
    int r,g,b;
    int res = 0;

    hsi2rgb(in_h, in_s, in_i, &r, &g, &b);

    stringstream ss;
    if (r != exp_r)
    {
        ss << "r (" << r << ") != exp_r (" << exp_r << ") ";
        res = 1;
    }
    if (g != exp_g)
    {
        ss << "g (" << g << ") != exp_g (" << exp_g << ") ";
        res = 1;
    }
    if (b != exp_b)
    {
        ss << "b (" << b << ") != exp_b (" << exp_b << ") ";
        res = 1;
    }

    if (res)
    {
        printf("Error in UT_hsi2rgb(%f, %f, %f)\n", in_h, in_s, in_i);
        cout << ss.str() << endl << endl;
    }

    return res;
}

void TH_rgb_hsi_conversion()
{
    int res = 0;

    // RGB to HSI

    // HSI to RGB
#if 0
    res += UT_hsi2rgb(210, 33.3, 150, 100, 150, 200);
#endif

    res += UT_rgb2hsi2rgb(100, 150, 200);
    res += UT_rgb2hsi2rgb(150, 0, 0);
    res += UT_rgb2hsi2rgb(0, 150, 0);
    res += UT_rgb2hsi2rgb(255, 0, 0);
#if 1
    res += UT_rgb2hsi2rgb(255, 255, 255);
    res += UT_rgb2hsi2rgb(0, 0, 0);
#endif
    res += UT_rgb2hsi2rgb(100, 150, 200);
    res += UT_rgb2hsi2rgb(100, 200, 150);

    // Special
    res += UT_rgb2hsi(0, 0, 255, 240, 100, 85);
    res += UT_hsi2rgb(240, 100, 85, 0, 0, 255);
    res += UT_rgb2hsi2rgb(0, 0, 255);

    // Print results
    if (res)
    {
        printf("TH_rgb_hsi_conversion(): Test/s failed = %d\n", res);
    }
    else
    {
        printf("TH_rgb_hsi_conversion(): All test/s passed.\n");
    }
}

int main(void)
{
    TH_rgb_hsi_conversion();
    return 0;
}
