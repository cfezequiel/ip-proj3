#ifndef INT3D_H
#define INT3D_H

#include <cstring>
#include <vector>

class Int3d
{
public:
    Int3d(): _sx(0), _sy(0), _sz(0) {}
    Int3d(size_t sx, size_t sy, size_t sz, int init=0);
    void assign(size_t sx, size_t sy, size_t sz, int init=0);
    int &operator()(size_t x, size_t y, size_t z);
    int size(int c);

private:
    std::vector<int> _a;
    size_t _sx;
    size_t _sy;
    size_t _sz;
};

#endif // INT3D_H
