#include <cstring>
#include "common.h"
#include "int3d.h"

void Int3d::assign(size_t sx, size_t sy, size_t sz, int init)
{
    _a.assign(sx * sy * sz, init);
}

Int3d::Int3d(size_t sx, size_t sy, size_t sz, int init)
{
    _sx = sx;
    _sy = sy;
    _sz = sz;

    Int3d::assign(sx, sy, sz, init);
}

int & Int3d::operator()(size_t x, size_t y, size_t z)
{
    return _a.at(x + y * _sx + z * _sx * _sy);
}

int Int3d::size(int c)
{
    int out;
    switch (c)
    {
        case 0: case 'x':
            out = _sx;
            break;
        case 1: case 'y':
            out =_sy;
            break;
        case 2: case 'z':
            out = _sz;
            break;
        default:
            out = -1;
    }

    return out;
}


