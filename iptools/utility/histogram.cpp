#include <cstdlib>
#include <cmath>
#include "histogram.h"

// Get maximum value in a vector of integers
int maxval(vector<int> &v)
{
    vector<int>::iterator it;
    int max = 0;
    for (it = v.begin(); it != v.end(); it++)
    {
        if (*it > max)
        {
            max = *it;
        }
    }
    return max;
}

Histogram::Histogram(Image &src, ROI roi, int channel)
{
    _hsize = G_RANGE;

    // Initialize histogram values to zero
    _h.assign(_hsize, 0);

    int gp;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            gp = src.getPixel(i, j, channel);
            _h[gp]++;
        }
    }

    _hmax = maxval(_h);
}

// FIXME: combine this with Histogram(Image ...) constructor
Histogram::Histogram(ImageHSI &src, ROI roi, int channel)
{
    // Set the histogram range depending on the type of HSI channel
    switch (channel)
    {
        case HUE:
            _hsize = H_MAX + 1;
            break;
        case SATURATION:
            _hsize = S_MAX + 1;
            break;
        case INTENSITY:
            _hsize = I_MAX + 1;
            break;
        default:
            throw IPException("Unrecognized channel value");
    }

    // Initialize histogram values to zero
    _h.assign(_hsize, 0);

    int gp;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            gp = src.getPixel(i, j, channel);
            if (gp < 0 || gp >= _hsize)
                continue;
            _h[gp]++;
        }
    }

    _hmax = maxval(_h);
}

Histogram::Histogram(video &vid, ROI roi, int channel, SOI soi)
{
    _hsize = G_RANGE;
    _h.assign(_hsize, 0);

    frame *pf;
    int gp;
    for (int s = soi.beg; s <= soi.end; s++)
    {
        pf = vid.getFrame(s);
        for (int i = roi.y; i < roi.y + roi.sy; i++)
        {
            for (int j = roi.x; j < roi.x + roi.sx; j++)
            {
                gp = pf->getPixel(i, j, channel);
                _h[gp]++;
            }
        }
    }

    _hmax = maxval(_h);
}

Histogram::Histogram(video &vid, ROI roi, HSICHANNEL channel, SOI soi)
{
    // Set the histogram range depending on the type of HSI channel
    switch (channel)
    {
        case HUE:
            _hsize = H_MAX + 1;
            break;
        case SATURATION:
            _hsize = S_MAX + 1;
            break;
        case INTENSITY:
            _hsize = I_MAX + 1;
            break;
        default:
            throw IPException("Unrecognized channel value");
    }
    _h.assign(_hsize, 0);

    frame *pf;
    int gp;
    for (int s = soi.beg; s < soi.end; s++)
    {
        pf = vid.getFrame(s);
        ImageHSI img(*pf);
        for (int i = roi.y; i < roi.y + roi.sy; i++)
        {
            for (int j = roi.x; j < roi.x + roi.sx; j++)
            {
                gp = img.getPixel(i, j, channel);
                _h[gp]++;
            }
        }
    }

    _hmax = maxval(_h);
}

Histogram::~Histogram()
{
    //do nothing?
}

vector<int> Histogram::pixGraph()
{
    int hx;
    vector<int> nh(_hsize);
    int fh;

    // Normalize histogram values
    for (int i = 0; i < _hsize; i++)
    {
        fh = _h[i];
        nh[i] = (int) (((float) fh / _hmax) * _hsize);
    }
        
    vector<int> himg(_hsize * _hsize);
    for (int j = 0; j < _hsize; j++)
    {
        hx = nh[j];
        // Set histogram pixel values as foreground color
        for (int i = _hsize - 1; i >= _hsize - hx; i--)
        {
                himg[i * _hsize + j] = 1;
        }
        // Set other pixel values as background color
        for (int i = _hsize - hx - 1; i >= 0; i--)
        {
                himg[i * _hsize + j] = 0;
        }
    }

    return himg;
}

void Histogram::overlayGraph(Image &tgt, ROI roi, int rgbColor)
{
    assert(roi.sx >= _hsize);
    assert(roi.sy >= _hsize);
    
    // Generate histogram graph pseudo-Image 
    vector<int> himg = pixGraph();

    // Unpack RGB colors
    int rgb[NCHANNELS];
    for (int ch = NCHANNELS - 1; ch >=0; ch--)
    {
        rgb[ch] = rgbColor & 0xff;
        rgbColor >>= 8;
    }

#if 0 //FIXME: apply scaling to histogram overlay
    // Scale histogram pseudo-Image to fit in ROI
    int dx = tgt.getNumberOfColumns() / _hsize;
    int dy = tgt.getNumberOfRows() / _hsize;
    for (int i = 0; i < _hsize; i++)
    {
        for (int j = 0; j < _hsize; j++)
        {
            int hp = himg[i * _hsize + j];
            for (int ii = 0; ii < dy; ii++)
            {
                for (int jj = 0; jj < dx; jj++)
                {
                    tgt.setPixel((roi.y + i) * dy + ii, (roi.x + j) * dx + jj, rp, hp); 
                    tgt.setPixel((roi.y + i) * dy + ii, (roi.x + j) * dx + jj, gp, hp); 
                    tgt.setPixel((roi.y + i) * dy + ii, (roi.x + j) * dx + jj, bp, hp); 
                }
            }
        }
    }
#else // Overlay works only for _hsize x _hsize ROIs
    // Start at the bottom left
    int hp, ii, jj;
    for (int i = _hsize - 1; i >= 0; i--)
    {
        for (int j = 0; j < _hsize; j++)
        {
            hp = himg[i * _hsize + j];

            // Overlay histogram pixels with a semblance of transparency
            if (hp && (rand() % 2 == 0))
            {
                ii = roi.y + roi.sy - 1 - (_hsize - i - 1);
                jj = roi.x + j;
                tgt.setPixel(ii, jj, RED, rgb[0]); 
                tgt.setPixel(ii, jj, GREEN, rgb[1]); 
                tgt.setPixel(ii, jj, BLUE, rgb[2]); 
            }
        }
    }
#endif
}

//FIXME: combine with equalize(Image &img, ...)
//Need to refactor Image class
void Histogram::equalize(ImageHSI &img, ROI roi, int channel)
{
    // Form cumulative histogram
    int *hc = new int[_hsize];
    hc[0] = _h[0];
    for (int p = 1; p < _hsize; p++)
    {
        hc[p] = hc[p - 1] + _h[p];
    }

    // Compute equalization transform 
    int *T = new int[_hsize];
    int N = roi.sy;
    int M = roi.sx;
    float k = (float) (_hsize - 1) / (N * M);
    for (int p = 0; p < _hsize; p++)
    {
        T[p] = (int) round(k * hc[p]);
    }

    // Apply transform to ROI of image
    int gp, gq;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            gp = img.getPixel(i, j, channel);
            gq = T[gp];
            img.setPixel(i, j, channel, gq);
        }
    }

    delete hc;
    delete T;
}

void Histogram::equalize(Image &img, ROI roi, int channel)
{
    // Form cumulative histogram
    int *hc = new int[_hsize];
    hc[0] = _h[0];
    for (int p = 1; p < _hsize; p++)
    {
        hc[p] = hc[p - 1] + _h[p];
    }

    // Compute equalization transform 
    int *T = new int[_hsize];
    int N = roi.sy;
    int M = roi.sx;
    float k = (float) (_hsize - 1) / (N * M);
    for (int p = 0; p < _hsize; p++)
    {
        T[p] = (int) round(k * hc[p]);
    }

    // Apply transform to ROI of image
    int gp, gq;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            gp = img.getPixel(i, j, channel);
            gq = T[gp];
            img.setPixel(i, j, channel, gq);
        }
    }

    delete hc;
    delete T;
}

void Histogram::equalize(video &vid, ROI roi, int channel, SOI soi)
{
    // Form cumulative histogram
    int *hc = new int[_hsize];
    hc[0] = _h[0];
    for (int p = 1; p < _hsize; p++)
    {
        hc[p] = hc[p - 1] + _h[p];
    }

    // Compute equalization transform 
    int *T = new int[_hsize];
    int N = roi.sy;
    int M = roi.sx;
    double k = (double) (_hsize - 1) / (N * M * (soi.end - soi.beg + 1));
    for (int p = 0; p < _hsize; p++)
    {
        T[p] = (int) round(k * hc[p]);
    }

    // Apply transform to ROI of each frame in SOI of the video
    frame *pf;
    int gp, gq;
    for (int s = soi.beg - 1; s < soi.end; s++)
    {
        pf = vid.getFrame(s);
        for (int i = roi.y; i < roi.y + roi.sy; i++)
        {
            for (int j = roi.x; j < roi.x + roi.sx; j++)
            {
                gp = pf->getPixel(i, j, channel);
                gq = T[gp];
                pf->setPixel(i, j, channel, gq);
            }
        }
    }

    delete hc;
    delete T;
}

void Histogram::equalize(video &vid, ROI roi, HSICHANNEL channel, SOI soi)
{
    // Form cumulative histogram
    int *hc = new int[_hsize];
    hc[0] = _h[0];
    for (int p = 1; p < _hsize; p++)
    {
        hc[p] = hc[p - 1] + _h[p];
    }

    // Compute equalization transform 
    int *T = new int[_hsize];
    int N = roi.sy;
    int M = roi.sx;
    double k = (double) (_hsize - 1) / (N * M * (soi.end - soi.beg + 1));
    for (int p = 0; p < _hsize; p++)
    {
        T[p] = (int) round(k * hc[p]);
    }

    // Apply transform to ROI of each frame in SOI of the video
    frame *pf;
    int gp, gq;
    for (int s = soi.beg - 1; s < soi.end; s++)
    {
        pf = vid.getFrame(s);
        ImageHSI img(*pf);
        for (int i = roi.y; i < roi.y + roi.sy; i++)
        {
            for (int j = roi.x; j < roi.x + roi.sx; j++)
            {
                gp = img.getPixel(i, j, channel);
                gq = T[gp];
                img.setPixel(i, j, channel, gq);
            }
        }
        // FIXME: refactor
        //img.toRGB(*pf, roi.x, roi.y, roi.sx, roi.sy);
    }

    delete hc;
    delete T;
}

