#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <assert.h>
#include <vector>
#include "common.h"
#include "ipparams.h"
#include "image.h"
#include "video.h"

class Histogram
{
    private:
        vector<int> _h;
        int _hmax;
        int _hsize;
        
    public:
        Histogram(Image &src, ROI roi, int channel);
        Histogram(video &src, ROI roi, int channel, SOI soi);
        Histogram(video &vid, ROI roi, HSICHANNEL channel, SOI soi);
        void overlayGraph(Image &tgt, ROI roi, int rgbcolor);
        void equalize(Image &img, ROI roi, int channel);
        void equalize(video &vid, ROI roi, int channel, SOI soi);
        void equalize(video &vid, ROI roi, HSICHANNEL channel, SOI soi);
        vector<int> pixGraph();
        ~Histogram();
};

#endif //HISTOGRAM_H

