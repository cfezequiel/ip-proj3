#include <cmath>
#include <float.h>
#include <algorithm>
#include "common.h"
#include "edgedetector.h"
#ifdef DEBUG
#include <stdio.h>
#undef DEBUG
#endif

#define RAD360  (2 * PI)
#define RAD2DEG (180 / PI)
#define DEG2RAD (PI / 180)
#define DEGREES_MAX 90
#define DEGREES_MIN -90

int _cl_mask[2][9] = {{1, 2, 1, 0, 0, 0, -1, -2, -1}, // vertical
                      {-1,0, 1,-2, 0, 2, -1,  0,  1}}; // horizontal

void clear(Image &img, ROI roi, int ch)
{
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            img.setPixel(i, j, ch, 0);
        }
    }
}

void drawCircle(Image &img, ROI roi, double a, double b, double r, int color)
{
    double th;
    int x,y;
    double thickness = 1.5;
    for (int deg = 0; deg < 360; deg+=1)
    {
        for (double k = -thickness; k < thickness; k++) 
        {
            th = deg * DEG2RAD;
            x = (int) (a + (r + k) * cos(th));
            y = (int) (b + (r + k) * sin(th));
            if (x < roi.x || x >= roi.x + roi.sx || y < roi.y || y >= roi.y + roi.sy)
            {
                continue;
            }
            img.setPixel(y, x, color);
        }
    }
}

Sobel::Sobel(Image &src, int ch)
{
    _src = src;
    _ch = ch;
}

int Sobel::operator()(int i, int j, int d)
{
    int pixel;
    int x,y;
    int sum = 0;
    int m = 0;

    for (int k = -1; k <= 1; k++)
    {
        for (int l = -1; l <= 1; l++)
        {
            y = i + k;
            x = j + l;

            if (x < 0 || x >= _src.columns() || y < 0 || y >= _src.rows())
            {
                continue;
            }

            pixel = _src.getPixel(y, x, _ch);
            sum += _cl_mask[d][m++] * pixel;
        }
    }

    return sum;
}

//-------------------- EdgeDetector --------------------  
EdgeDetector::EdgeDetector(const Image &src, int ch, ROI roi)
{
    _src = src;
    _roi = roi;
    _ch = ch;
    _doDirectionThresholding = false;
}

void EdgeDetector::setGradientThreshold(int t) 
{
    _gt = t;
}

void EdgeDetector::setGradientDirection(int degrees) 
{
    // Range is [0, 360)
    if (degrees >= 0 && degrees < 360)
    {
        _gd = degrees * DEG2RAD;
        _doDirectionThresholding = true;
    }
    else
    {
        _doDirectionThresholding = false;
    }
}

void EdgeDetector::setLocalMaximaThreshold(int m) 
{
    _lmt = m;
}

void EdgeDetector::setHoughSpaceParams(int na, int nb, int nr, int maxr, int minr)
{
    if (na <= 0 || nb <= 0 || nr <= 0 || maxr <= 0 || minr <= 0)
    {
        throw IPException("Parameters must be positive integers");
    }

    if (maxr < minr)
    {
        throw IPException("Maximum radius must be greater than minimum radius.");
    }

    _na = na;
    _nb = nb;
    _nr = nr;
    _maxr = maxr;
    _minr = minr;

    // Set scaling factors
    _sa = _na / (double) _roi.sx;
    _sb = _nb / (double) _roi.sy;
    _sr =  (_maxr - _minr) / (double) _nr;
}

void EdgeDetector::_gradient()
{
    double gx, gy;
    double magnitude;
    double direction;

    _imgGradMag.copy(_src);
    _imgGradDir.copy(_src);
    if (_src.getColorspace() == CS_HSI)
    {
        clear(_imgGradMag, _roi, 0);
        clear(_imgGradMag, _roi, 1);
        clear(_imgGradDir, _roi, 0);
        clear(_imgGradDir, _roi, 1);
    }
    Sobel sobel(_src, _ch);

    for (int i = _roi.y; i < _roi.y + _roi.sy; i++)
    {
        for (int j = _roi.x; j < _roi.x + _roi.sx; j++)
        {
            gy = sobel(i, j, 0);
            gx = sobel(i, j, 1);
            magnitude = sqrt(gy * gy + gx * gx);
            direction = atan2(gy, gx);

            // Scale for display
            magnitude = magnitude > _src.maxval(_ch) ? _src.maxval(_ch) : magnitude;
            direction += direction < 0 ? RAD360: 0; // Change range to [0, 360)
            direction = (direction / RAD360) * _src.maxval(_ch);

#if DEBUG 
            printf("_gradient(): gy = %f, gx = %f, magnitude = %d, direction = %d\n", gy, gx, (int) magnitude, (int) direction);
#endif
            _imgGradMag.setPixel(i, j, _ch, (int) magnitude);
            _imgGradDir.setPixel(i, j, _ch, (int) direction);
        }
    }
}

void EdgeDetector::_threshold()
{
    double direction;
    int pixel;
    double offset = 5 * DEG2RAD;

    if (_imgGradMag.empty())
    {
        _gradient();
    }

    _imgEdge.copy(_imgGradMag);
    for (int i = _roi.y; i < _roi.y + _roi.sy; i++)
    {
        for (int j = _roi.x; j < _roi.x + _roi.sx; j++)
        {
            // Magnitude threshold
            pixel = _imgGradMag.getPixel(i, j, _ch);
#if DEBUG 
            printf("_threshold(): pixel= %d, threshold = %d\n", pixel, _gt);
#endif
            if (pixel >= _gt)
            {
                _imgEdge.setPixel(i, j, _ch, _src.maxval(_ch));
            }
            else
            {
                _imgEdge.setPixel(i, j, _ch, 0);
            }

            if (_doDirectionThresholding)
            {
                // Directional threshold
                direction = _imgGradDir.getPixel(i, j, _ch);
                direction = (direction / _src.maxval(_ch)) * RAD360;
                if (direction < _gd - offset || direction > _gd + offset)
                {
                    _imgEdge.setPixel(i, j, _ch, 0);
                }
            }
#if DEBUG 
            printf("_threshold(): gradient direction = %f, given direction= %f\n", 
                    direction, _gd);
#endif

        }
    }
}

void EdgeDetector::_houghTransform()
{
    // Generate Hough space image
    if (_imgEdge.empty())
    {
        _threshold();
    }

    // Hough accumulator
    _acc.assign(_nr, Image(_nb, _na));

    // Variables
    double a,b,r; 
    int a_, b_;
    double th;
    int pixel;
    int v;
    int edgeDir;

    _maxVotes.assign(_nr, 0);
    for (int i = _roi.y; i < _roi.y + _roi.sy; i++)
    {
        for (int j = _roi.x; j < _roi.x + _roi.sx; j++)
        {
            pixel = _imgEdge.getPixel(i, j, _ch);
            if (pixel)
            {
                // Get edge direction
                edgeDir = (int) (_imgGradDir.getPixel(i, j, _ch) * RAD2DEG);

                for (int k = 0; k < _nr; k++)
                {
                    r = _minr + (k + 1) * _sr;

#ifdef DUAL_DIRECTION
                    for (int flip = -1; flip <= 1; flip +=2)
                    {
#else
                        int flip = 1;
#endif
                        for (int deg = flip * edgeDir - 45; deg < flip * edgeDir + 45; deg+=3)
                        {
                            th = deg * DEG2RAD;
                            a = j - r * cos(th);
                            b = i - r * sin(th);
                            a_ = (int) ((a - _roi.x) * _sa);
                            b_ = (int) ((b - _roi.y) * _sb);
                            
                            if (a_ < 0 || a_ >= _na || b_ < 0 || b_ >= _nb)
                            {
                                continue;
                            }
                            v = _acc[k].getPixel(b_, a_, 0) + 1;
#if DEBUG 
                            printf("_houghTransform(): _acc(%d, %d, %d) = %d\n", k, b_, a_, v);
#endif
                            _acc[k].setPixel(b_, a_, 0, v);
                            if (v > _maxVotes[k])
                            {
                                _maxVotes[k] = v;
                            }
                        } // end for deg
#ifdef DUAL_DIRECTION
                    } // end for flip
#endif
                }
            }
        }
    }
}

void EdgeDetector::_overlayHoughSpace(Image &tgt)
{
    if (_acc.empty())
    {
        _houghTransform();
    }

    // Clear ROI of target image
    tgt.copy(_imgEdge);
    clear(tgt, _roi, _ch);

    // Generate merged Hough space image
    Image merge;
    merge.resize(_nb, _na);
    int max=1;
    for (int k = 0; k < _nr; k++)
    {
        for (int i = 0; i < _nb; i++)
        {
            for (int j = 0; j < _na; j++)
            {
                int c = merge.getPixel(i,j, 0) + _acc[k].getPixel(i,j,0);    
                merge.setPixel(i,j,0,c);
                if (c > max)
                {
                    max = c;
                }
            }
        }
    }

    // Overlay Hough space image
    int a,b;
    double scale = tgt.maxval(_ch) / (double) max;

    for (int i = 0; i < _nb; i++)
    {
        for (int j = 0; j < _na; j++)
        {
            int c = merge.getPixel(i, j, 0);
            if (c)
            {
                a = (int) (j / _sa + _roi.x);
                b = (int) (i / _sb + _roi.y);
                if (a < _roi.x || a >= _roi.x + _roi.sx ||
                        b < _roi.y || b >= _roi.y + _roi.sy)
                {
                    continue;
                }
                tgt.setPixel(b, a, _ch, c * scale);
            }
        }
    }
}

void EdgeDetector::_houghThreshold()
{
    if (_acc.empty())
    {
        _houghTransform();
    }

    int count;
    double threshold;

#define GLOBAL_THRESHOLD
#ifdef GLOBAL_THRESHOLD
    threshold = *max_element(_maxVotes.begin(), _maxVotes.end()); 
    threshold *= (0.01 * (double) _lmt);
#endif

    for (int k = 0; k < _nr; k++)
    {
#ifndef GLOBAL_THRESHOLD
        threshold = (double) _maxVotes[k] * (0.01 * (double) _lmt);
#endif
        for (int i = 0; i < _nb; i++)
        {
            for (int j = 0; j < _na; j++)
            {
                count = _acc[k].getPixel(i, j, 0);
                if (count >= threshold)
                {
#if DEBUG
                    printf("count = %d, threshold = %f @ (%d, %d, %d)\n", count, threshold, i,j,k);
#endif
                    _acc[k].setPixel(i, j, 0, _src.maxval(_ch));
                }
                else
                {
                    _acc[k].setPixel(i, j, 0, 0);
                }
            }
        }
    }
}

void EdgeDetector::_overlay()
{
    if (_acc.empty())
    {
        _houghThreshold();
    }
            
    // Get circle color
    int color;
    if (_src.getColorspace() == CS_GREY)
    {
        color = G_MAX;
    }
    else if (_src.getColorspace() == CS_HSI)
    {
        color = HSICOLOR_YELLOW;
    }

    // Draw the detected circles
    _imgOverlay.copy(_src);
    int count;
    double a,b,r;
    for (int k = 0; k < _nr; k++)
    {
        for (int i = 0; i < _nb; i++)
        {
            for (int j = 0; j < _na; j++)
            {
                count = _acc[k].getPixel(i, j, 0);
#if 0
                printf("_overlay(): i = %d, j = %d, count = %d\n", i, j, count);
#endif 
                if (count)
                {
                    // Get radius in image space
                    r = _minr + (k + 1) * _sr;

                    // Get center in image space
                    a = j / _sa + _roi.x;
                    b = i / _sb + _roi.y;

                    if (a < _roi.x || a >= _roi.x + _roi.sx ||
                            b < _roi.y || b >= _roi.y + _roi.sy)
                    {
                        continue;
                    }
#if 0 
                    printf("_overlay(): a = %d, b = %d\n", a, b);
#endif

                    // Draw circle
                    drawCircle(_imgOverlay, _roi, a, b, r, color);
                }
            }
        }
    }
}

Image * EdgeDetector::getGradMagImage()
{
    if (_imgGradMag.empty())
    {
        _gradient();
    }

    return &_imgGradMag;
}

Image * EdgeDetector::getGradDirImage()
{
    if (_imgGradDir.empty())
    {
        _gradient();
    }

    return &_imgGradDir;
}

Image * EdgeDetector::getEdgeImage()
{
    if (_imgGradDir.empty())
    {
        _gradient();
    }

    _threshold();

    return &_imgEdge;
}

Image * EdgeDetector::getHoughSpaceImage()
{
    if (_imgHoughSpace.empty())
    {
        _overlayHoughSpace(_imgHoughSpace);
    }

    return &_imgHoughSpace;
}

Image * EdgeDetector::getThreshHoughSpaceImage()
{
    if (_imgThreshHoughSpace.empty())
    {
        _houghThreshold();
        _overlayHoughSpace(_imgThreshHoughSpace);
    }

    return &_imgThreshHoughSpace;
}

Image * EdgeDetector::getEnhancedImageOverlay()
{
    if (_imgOverlay.empty())
    {
        _overlay();
    }

    return &_imgOverlay;
}


