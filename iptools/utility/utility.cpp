#include <assert.h>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <strings.h>
#include <sstream>
#include <string.h>
#include <time.h>
#include "common.h"
#include "utility.h"
#include "histogram.h"
#include "edgedetector.h"
using namespace std;

#define STRLEN 256

/**
 * Get file extension
 */
string getext(string path)
{
    int dotPos = path.find_last_of(".");

    // Get extension without the "."
    string ext = path.substr(dotPos + 1);

    return ext;
}

/**
 * Checks if ROI is within an image
 */
bool roiInImage(Image &img, ROI roi)
{
    int xmax;
    int ymax;
    int imgWidth = img.columns();
    int imgHeight = img.rows();

    xmax = roi.x + roi.sx;
    ymax = roi.y + roi.sy;

    if (roi.x < 0 || roi.y < 0 || xmax > imgWidth || ymax > imgHeight)
    {
        return false;
    }

    return true;
}

#if 0 //TODO: refactor

/**
 * Calculates the Euclidean distance between two vectors
 */
int eucdist(int *p1, int *p2, int dim)
{
    assert(dim > 0);
    assert(p1 != NULL);
    assert(p2 != NULL);

    int d = 0;
    for (int i = 0; i < dim; i++)
    {
        d += (p1[i] - p2[i]) * (p1[i] - p2[i]);
    }
    d = (int) ceil(sqrt(d));

    return d;
}


/**
 * Compute the window mean
 */
int computeWindowMean(Image &img, int hs, int vs, int i, int j)
{
    int windowMean;
    int p;
    int q;
    int n;
    int imgWidth = img.columns();
    int imgHeight = img.rows();

    // Assume odd window size (center-reference)
    assert(hs > 0);
    assert(vs > 0);

    // Compute window mean
    windowMean = 0;
    for (int m = -hs; m <= hs; m++)
    {
        for (int n = -vs; n <= vs; n++)
        {
            p = i + m;
            q = j + n;
            if (p < 0 || p >= imgWidth || q < 0 || q >= imgHeight)
            {
                continue;
            }
            windowMean += img.getPixel(p, q);
        }
    }
    n = (hs * 2 + 1) * (vs * 2 + 1);
    windowMean = (windowMean + n) / n;

    return windowMean;
}

/*-----------------------------------------------------------------------**/
void utility::greyThresh(Image &src, Image &tgt, IPParam p)
{
    int pixValue;
    int threshold;

    // Run algorithm on ROI
    threshold = p.filter_params[0];
    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::colorThresh(Image &src, Image &tgt, IPParam p)
{
    int threshold;
    int color[NCHANNELS];
    int pixel[NCHANNELS];

    // Get threshold value for ROI
    threshold = p.filter_params[0];

    // Get color comparison value for ROI
    color[0] = p.filter_params[1];
    color[1] = p.filter_params[2];
    color[2] = p.filter_params[3];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixel[0] = src.getPixel(i, j, RED);
            pixel[1] = src.getPixel(i, j, GREEN);
            pixel[2] = src.getPixel(i, j, BLUE);

            if (eucdist(pixel, color, 3) <= threshold)
            {
                tgt.setPixel(i, j, RED, 255);
                tgt.setPixel(i, j, GREEN, 255);
                tgt.setPixel(i, j, BLUE, 255);
            }
            else
            {
                tgt.setPixel(i, j, RED, 0);
                tgt.setPixel(i, j, GREEN, 0);
                tgt.setPixel(i, j, BLUE, 0);
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::adaptiveThresh(Image &src, Image &tgt, IPParam p)
{
    int threshold;
    int windowSize;
    int pixValue;
    int weight;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    // Get weight value for ROI
    weight = p.filter_params[1];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);

            // Set threshold
            threshold = windowMean + weight;

            // Apply threshold
            pixValue = src.getPixel(i, j);
            if (pixValue >= threshold)
            {
                pixValue = 255;
            }
            else
            {
                pixValue = 0;
            }
            tgt.setPixel(i, j, pixValue);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing2D(Image &src, Image &tgt, IPParam p)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, windowSize, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::smoothing1D(Image &src, Image &tgt, IPParam p)
{
    int windowSize;
    int windowMean;

    // Get window size for ROI
    windowSize = p.filter_params[0];

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Process horizontal window averaging
            windowMean = computeWindowMean(src, windowSize, 1, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }

    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            // Process vertical window averaging
            windowMean = computeWindowMean(tgt, 1, windowSize, i, j);
            tgt.setPixel(i, j, windowMean);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::scale(Image &src, Image &tgt, float value)
{
    int width = src.rows();
    int height = src.columns();

    tgt.resize(width * value, height * value);

    // Scale down
    if (value < 1)
    {
        for (int i = 0; i < tgt.rows(); i++)
        {
            for (int j = 0; j < tgt.columns(); j++)
            {
                int sum = 0;
                int ave;
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        sum += src.getPixel(i / value + ii, j / value + jj);
                        ave = sum / value;
                    }
                }
                tgt.setPixel(i, j, ave);
            }
        }
    }
    // Scale Up
    else
    {
        for (int i = 0; i<width; i++)
        {
            for (int j = 0; j<height; j++)
            {
                int pixel = src.getPixel(i, j); 
                for (int ii = 0; ii < value; ii++)
                {
                    for (int jj = 0; jj < value; jj++)
                    {
                        tgt.setPixel(i * value + ii, j * value + jj, pixel); 
                    }
                }
            }
        }
    } // end if-else
}

#endif

/*-----------------------------------------------------------------------**/
#if 0 // FIXME: Refactor
void utility::histogram(Image &src, Image &tgt, IPParam p)
{
    assert(p.filter == "histogram");

    int channel;
    ImageHSI *hsiImage;
    Histogram *h;

    // Clear the ROI
    utility::clear(tgt, p.roi);

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(src, p.roi, GREY);
            h->overlayGraph(tgt, p.roi, RGBCOLOR_GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(src, p.roi, RED);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(src, p.roi, GREEN);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(src, p.roi, BLUE);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            // Convert Image to HSI color space
            hsiImage = new ImageHSI((ImageRGB) src);

            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(*hsiImage, p.roi, HUE);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_YELLOW);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(*hsiImage, p.roi, SATURATION);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_CYAN);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(*hsiImage, p.roi, INTENSITY);
                h->overlayGraph(tgt, p.roi, RGBCOLOR_MAGENTA);
                delete h;
            }

            delete hsiImage;
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}

/*-----------------------------------------------------------------------**/
void utility::histogram(video &vid, Image &img, IPParam p)
{
    assert(p.filter == "histogram");

    Histogram *h;
    int channel;
    frame *frm;

    // Set the output image
    if (img.isEmpty())
    {
        // Get first frame of first SOi of video
        frm = vid.getFrame(p.soi.beg);
        img.copyImage(*frm);
    }
    utility::clear(img, p.roi);

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(vid, p.roi, GREY, p.soi);
            h->overlayGraph(img, p.roi, RGBCOLOR_GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(vid, p.roi, RED, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(vid, p.roi, GREEN, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(vid, p.roi, BLUE, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_YELLOW);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_CYAN);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                h->overlayGraph(img, p.roi, RGBCOLOR_MAGENTA);
                delete h;
            }
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}
/*-----------------------------------------------------------------------**/
void utility::histoEqualize(Image &src, Image &tgt, IPParam p)
{
    assert(p.filter == "histoequalize");

    int channel;
    ImageHSI *hsiImage;
    Histogram *h;

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(src, p.roi, GREY);
            h->equalize(tgt, p.roi, GREY);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(src, p.roi, RED);
                h->equalize(tgt, p.roi, RED);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(src, p.roi, GREEN);
                h->equalize(tgt, p.roi, GREEN);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(src, p.roi, BLUE);
                h->equalize(tgt, p.roi, BLUE);
                delete h;
            }
            break;

        case CS_HSI:
            // Convert Image to HSI color space
            hsiImage = new ImageHSI(src);

            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(*hsiImage, p.roi, HUE);
                h->equalize(*hsiImage, p.roi, HUE);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(*hsiImage, p.roi, SATURATION);
                h->equalize(*hsiImage, p.roi, SATURATION);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(*hsiImage, p.roi, INTENSITY);
                h->equalize(*hsiImage, p.roi, INTENSITY);
                delete h;
            }

            // Convert Image back to RGB space
            hsiImage->toRGB(tgt, p.roi.x, p.roi.y, p.roi.sx, p.roi.sy);

            delete hsiImage;
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}


/*-----------------------------------------------------------------------**/
void utility::histoEqualize(video &vid, IPParam p)
{
    assert(p.filter == "histoequalize");

    Histogram *h;
    int channel;

    switch (p.filter_params[0])
    {
        case CS_GREY:
            // Generate histogram for grey channel
            h = new Histogram(vid, p.roi, GREY, p.soi);
            h->equalize(vid, p.roi, GREY, p.soi);
            delete h;
            break;

        case CS_RGB:
            channel = p.filter_params[1];
            if (channel & RED)
            {
                // Generate histogram for red channel
                h = new Histogram(vid, p.roi, RED, p.soi);
                h->equalize(vid, p.roi, RED, p.soi);
                delete h;
            }
            if (channel & GREEN)
            {
                // Generate histogram for green channel
                h = new Histogram(vid, p.roi, GREEN, p.soi);
                h->equalize(vid, p.roi, GREEN, p.soi);
                delete h;
            }
            if (channel & BLUE)
            {
                // Generate histogram for blue channel
                h = new Histogram(vid, p.roi, BLUE, p.soi);
                h->equalize(vid, p.roi, BLUE, p.soi);
                delete h;
            }
            break;

        case CS_HSI:
            channel = p.filter_params[1];
            if (channel & HUE)
            {
                // Generate histogram for hue channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) HUE, p.soi);
                delete h;
            }
            if (channel & SATURATION)
            {
                // Generate histogram for saturation channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) SATURATION, p.soi);
                delete h;
            }
            if (channel & INTENSITY)
            {
                // Generate histogram for intensity channel
                h = new Histogram(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                h->equalize(vid, p.roi, (HSICHANNEL) INTENSITY, p.soi);
                delete h;
            }
            break;

        default:
            throw IPException("Unknown colorspace.");
    }
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(Image &src, string tgtPath, IPParam p)
{
    int pixValue;

    string ext;
    COLORSPACE cs = src.getColorspace();
    if (cs == CS_GREY)
    {
        ext.assign(".pgm");
    }
    else
    {
        throw IPException("Not a greyscale image");
    }

    // Run algorithm
    int weight = p.filter_params[0];
    for (int i = p.roi.y; i < p.roi.y + p.roi.sy; i++)
    {
        for (int j = p.roi.x; j < p.roi.x + p.roi.sx; j++)
        {
            pixValue = src.getPixel(i, j, 0) + weight;
            if (pixValue > 255)
            {
                pixValue = 255;
            }
            else if (pixValue < 0)
            {
                pixValue = 0;
            }
            else 
            {
                pixValue = src.getPixel(i, j, 0);
            }
            tgt.setPixel(i, j, 0, pixValue);
        }
    }

    save(tgt, tgtPath.append(ext).c_str());
}

#endif

/*-----------------------------------------------------------------------**/
//
// Clear image channel
//
void utility::clear(Image &tgt, ROI roi, int ch)
{
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            tgt.setPixel(i, j, ch, 0);
        }
    }
}


/*-----------------------------------------------------------------------**/
void utility::edgeDetect(Image &src, string tgtPath, IPParam p)
{
    char filename[STRLEN];
    string ext;
    ext = src.getColorspace() == CS_GREY ? "pgm" : "ppm";

    // Unpack parameters
    int edgeThreshold = p.filter_params[0];
    int edgeDirection = p.filter_params[1];
    int houghThreshold = p.filter_params[2];
    int na = p.filter_params[3];
    int nb = p.filter_params[4];
    int nr = p.filter_params[5];
    int maxCircleRadius = p.filter_params[6];
    int minCircleRadius = p.filter_params[7];

    // Create image pointer
    Image *tgt;

    // Create edge detector
    // Use I channel if not greyscale
    int ch;
    if (src.getColorspace() == CS_GREY)
    {
        ch = 0;
    }
    else
    {
        ch = 2;
        // Clear all channels except 'I' for HSI
        //utility::clear(src, p.roi, 0);
        //utility::clear(src, p.roi, 1);
    }
    EdgeDetector ed(src, ch, p.roi);

    // Set edge detection parameters
    ed.setGradientThreshold(edgeThreshold);
    ed.setGradientDirection(edgeDirection);
    ed.setLocalMaximaThreshold(houghThreshold);
    ed.setHoughSpaceParams(na, nb, nr, maxCircleRadius, minCircleRadius);
    
    // Get gradient magnitude image
    tgt = ed.getGradMagImage();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "grad", ext.c_str());
    save(*tgt, filename);

    // Get gradient direction image
    tgt = ed.getGradDirImage();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "dir", ext.c_str());
    save(*tgt, filename);

    // Get edge image (gradient image after thresholding both magnitude and
    // direction)
    tgt = ed.getEdgeImage();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "edge", ext.c_str());
    save(*tgt, filename);

    // Get hough transform space as image
    tgt = ed.getHoughSpaceImage();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "hough", ext.c_str());
    save(*tgt, filename);

    // Get thresholded hough space as image
    tgt = ed.getThreshHoughSpaceImage();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "center", ext.c_str());
    save(*tgt, filename);

    // Overlay enhanced image on source image
    tgt = ed.getEnhancedImageOverlay();
    snprintf(filename, STRLEN, "%s_%s.%s", tgtPath.c_str(), "detected", ext.c_str());
    save(*tgt, filename);
}

/*-----------------------------------------------------------------------**/
void utility::processImage(string srcPath, string tgtPath, vector<IPParam> params)
{
    string ext;
    Image *src;

    // Process each parameter line
    vector<IPParam>::iterator iparam;
    for (iparam = params.begin(); iparam != params.end(); iparam++)
    {
        // Execute filter
        if (iparam->filter == "edgedetect")
        {
            ext = getext(srcPath);
            if (ext == "ppm")
            {
                src = load(srcPath.c_str(), CS_HSI);
            }
            else
            {
                src = load(srcPath.c_str(), CS_GREY);
            }

            if (!roiInImage(*src, iparam->roi))
            {
                throw IPException("Given ROI is not within image.");
            }

            utility::edgeDetect(*src, tgtPath, *iparam);
        }
        else
        {
            stringstream m;
            m << "Unrecognized filter: " << iparam->filter << '.';
            throw IPException(m.str().c_str());
        }
    }
}

void utility::processVideo(string srcPath, string tgtPath, vector<IPParam> ps,
        COLORSPACE cs, VID_MODE mode)
{
    //TODO: refactor
}


