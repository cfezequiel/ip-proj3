#ifndef UTILITY_H
#define UTILITY_H

#include "../ipparams/ipparams.h"
#include "../video/video.h"
#include "../image/image.h"
#include <sstream>

enum VID_MODE
{
    PER_FRAME = 0,
    PER_SOI = 1
};

class utility
{
	public:
		utility();
		virtual ~utility();
		static std::string intToString(int number);
        static void clear(Image &tgt, ROI roi, int ch);
        static void addGrey(Image &src, Image &tgt, IPParam p);
        static void greyThresh(Image &src, Image &tgt, IPParam p);
        static void colorThresh(Image &src, Image &tgt, IPParam p);
        static void scale(Image &src, Image &tgt, float value);
        static void adaptiveThresh(Image &src, Image &tgt, IPParam p);
        static void smoothing2D(Image &src, Image &tgt, IPParam p);
        static void smoothing1D(Image &src, Image &tgt, IPParam p);
        static void histogram(Image &src, Image &tgt, IPParam p);
        static void histogram(video &vid, Image &img, IPParam p);
        static void histoEqualize(Image &src, Image &tgt, IPParam p);
        static void histoEqualize(video &vid, IPParam p);
        static void edgeDetect(Image &src, string tgtPath, IPParam p);
        static void processImage(string srcPath, string tgtPath, vector<IPParam> params);
        static void processVideo(string srcPath, string tgtPath, vector<IPParam> ps,
                    COLORSPACE cs, VID_MODE mode);
        static void processVideo(string src, Image &tgt, vector<IPParam> ps,
                COLORSPACE cs, VID_MODE mode);
};

string getext(string path);

#endif

