#ifndef EDGEDETECTOR_H
#define EDGEDETECTOR_H

#include "../ipparams/ipparams.h"
#include "../image/image.h"
#include "int3d.h"

enum STAGE
{
    STAGE_GRADIENT = 0x0,
    STAGE_EDGE     = 0x1,
    STAGE_HOUGH    = 0x2,
    STAGE_DETECT   = 0x3
};

class Sobel
{
public:
    Sobel(Image &src, int ch);
    int operator()(int i, int j, int d);

private:
    Image _src;
    int _ch;
};

class EdgeDetector
{
public:
    EdgeDetector(const Image &src, int ch, ROI roi);

    void setGradientThreshold(int t);    
    void setGradientDirection(int d);   
    void setLocalMaximaThreshold(int m);
    void setHoughSpaceParams(int na, int nb, int nr, int maxr, int minr);

    Image *getGradMagImage();
    Image *getGradDirImage();
    Image *getEdgeImage();
    Image *getHoughSpaceImage();
    Image *getThreshHoughSpaceImage();
    Image *getEnhancedImageOverlay();

private:
    // Image selection
    Image _src;
    ROI _roi;
    int _ch;

    // User-specified parameters
    int _gt;
    float _gd;
    int _lmt;
    int _na;
    int _nb;
    int _nr;
    int _minr;
    int _maxr;

    // Derived parameters
    bool _doDirectionThresholding;
    int _ddeg;
    vector<int> _maxVotes;
    double _sa;
    double _sb;
    double _sr;

    // Processed images
    Image _imgGradMag;
    Image _imgGradDir;
    Image _imgEdge;
    Image _imgEdgeDir;
    Image _imgHoughSpace;
    Image _imgThreshHoughSpace;
    Image _imgOverlay;

    // Utility
    vector<Image> _acc;
    void _gradient();
    void _threshold();
    void _houghTransform();
    void _overlay();
    void _overlayHoughSpace(Image &tgt);
    void _houghThreshold();
};

#endif //EDGEDETECTOR_H
