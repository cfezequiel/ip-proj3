#include <assert.h>
#include <dirent.h>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <algorithm>
#include "../common.h"
#include "video.h"
using namespace std;

int runcmd(const stringstream &cmd)
{
    string cmdstr(cmd.str());
    return system(cmdstr.c_str());
}

void frame::setPixel(int row, int col, int color)
{
    // Assume RGB space
    if (getColorspace() == CS_RGB)
    {
        int r,g,b;
        b = color & 0xff;
        g = (color >> 8) & 0xff;
        r = (color >> 16) & 0xff;

        Image::setPixel(row, col, 0, r);
        Image::setPixel(row, col, 1, g);
        Image::setPixel(row, col, 2, b);
    }
}

bool frameCompare(frame *left, frame *right)
{
    cout << "left = " << left << " right = " << right << endl;
    if (!left || !right)
    {
        return false;
    }

    return (*left).path.compare((*right).path);
}

frame::frame(string path, int seqNum)
{
    frame::assign(path, seqNum);
}

void frame::assign(string path, int seqNum)
{
    this->path = path;
    this->seqNum = seqNum;
}

video::video()
{
    // do nothing
}

video::~video()
{
    _frames.clear();
}

video::video(string filename, COLORSPACE cs)
{
    this->loadVideo(filename, cs);
}

void video::loadVideo(string filename, COLORSPACE cs)
{
    int err;
    string ext;
    stringstream cmd;
    DIR *dp;
    struct dirent *de;

    // Clear frame buffer if not empty
    if (!_frames.empty())
    {
        _frames.clear();
    }

    // Set image file extension
    if (cs == CS_GREY)
    {
        ext.assign("pgm");
    }
    else if (cs == CS_RGB)
    {
        ext.assign("ppm");
    }

    // Generate temporary frame directory
    stringstream frameDir;
    frameDir << "tmp_" << rand();
    frameDir >> this->_frameDir;
    cmd << "mkdir -p " << this->_frameDir;
    err = runcmd(cmd);
    if (err)
    {
        throw IPException("Could not create video frame directory.");
    }

    // Extract frames from video file into frame directory
    stringstream ss;
    ss << "img%03d." << ext;
    cmd.str(string());
    cmd.clear();
    cmd << "ffmpeg -v error -i " << filename << ' ' << this->_frameDir << '/' <<
        ss.str();
    err = runcmd(cmd);
    if (err)
    {
        throw IPException("Failed to extract video frames.");
    }
    this->_frameFileFormat = ss.str();

    // Open frame directory
    dp = opendir(this->_frameDir.c_str());
    if (dp == NULL)
    {
        throw IPException("Failed to open frame directiory.");
    }

    // Read frame names
    vector<string> framePaths;
    while (true)
    {
        de = readdir(dp);
        if (de == NULL)
        {
            break;
        }
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
        {
            continue;
        }

        framePaths.push_back(string(de->d_name));
    }
    closedir(dp);

    // Sort frame names
    sort(framePaths.begin(), framePaths.end());

    // Generate frame objects
    _frames.assign(framePaths.size(), frame());

    string name;
    stringstream framePath;
    vector<string>::iterator iter;
    Image *ip;
    for (unsigned int i = 0; i < framePaths.size(); i++)
    {
        // Assemble full path to frame file
        framePath << this->_frameDir << '/' << framePaths[i];
        name.assign(framePath.str());
        framePath.str(string());
        framePath.clear();

        // Read frame file
        ip = load(name.c_str(), CS_RGB);
        _frames[i].assign(name, i + 1);
        _frames[i].copy(*ip);
        delete ip;
    }
}

void video::saveVideo(string filename)
{
    stringstream cmd;

    // Make sure that the 'load' function was used properly
    if (_frameDir.empty() || _frames.empty())
    {
        throw IPException("No frames to save.");
    }

    // Save each frame to corresponding frame file
    for (int i = 0; i < getFrameCount(); i++)
    {
        save(_frames[i], _frames[i].path.c_str());
    }

    // Compile all frames in frame directory 
    cmd << "ffmpeg -y -v quiet -i " << this->_frameDir << '/' <<
        this->_frameFileFormat << ' ' << filename;
    int err = runcmd(cmd);
    if (err)
    {
        throw IPException("Could not generate frames from vidoe.");
    }

}

void video::close()
{
    if (this->_frameDir.empty())
    {
        throw IPException("Video was not open.");
    }

    // Remove temporary frame directory
    stringstream cmd;
    cmd << "rm -r " << this->_frameDir;
    int err = runcmd(cmd);
    if (err)
    {
        throw IPException("Unable to remove temporary frame directory.");
    }
}

int video::getFrameCount()
{
    return _frames.size();
}

frame * video::getFrame(int i)
{
    assert(i < getFrameCount());

    return &_frames[i];
}

void video::setFrame(int i, Image &img)
{
    assert(i < getFrameCount());

    _frames[i].copy(img);
}


