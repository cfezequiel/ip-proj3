#ifndef VIDEO_H
#define VIDEO_H

#include <vector>
#include <string>
#include "../image/image.h"

class frame: public Image
{
    public:
        int seqNum;
        string path;

        frame() : Image() {}
        frame(string path, int seqNum);
        frame(const frame &f) {}
        void assign(string path, int seqNum);
        void setPixel(int row, int col, int color);
        void setPixel(int row, int col, int ch, int value);
};

class video 
{
	private: 
        string _frameFileFormat;
        string _frameDir;
        vector<frame> _frames;

	public: 

		video();
        video(string filename, COLORSPACE cs);
		~video();

        void loadVideo(string filename, COLORSPACE cs);
        void saveVideo(string filename);
        void close();
        int getFrameCount();
        frame * getFrame(int i);
        void setFrame(int i, Image &img);
};

#endif

